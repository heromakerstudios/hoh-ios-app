//
//  SuperHeroName.swift
//  HoH App
//
//  Created by Omar Alejandro Regalado Mendoza on 28/01/22.
//

import SwiftUI

struct SuperHeroName: View {
    
    @ObservedObject private var shnVM = SuperHeroNameViewModel()
    @State private var isActive = false
    @SwiftUI.Environment (\.presentationMode) private var presentationMode
    
    var body: some View {
        NavigationView {
            VStack {
                    
                HStack(alignment: .center) {
                    Text(SuperHeroNameLocalized.screenTitle)
                                        .font(.system(size: 24))
                                        .fontWeight(.medium)
                }
                .padding(.top, 69)
                
                Text(SuperHeroNameLocalized.screenSubTitle)
                    .font(.system(size: 14))
                    .fontWeight(.medium)
                    .padding(.top, 120)
                
                Text(SuperHeroNameLocalized.screenBody)
                    .font(.system(size: 14))
                    .fontWeight(.light)
                    .multilineTextAlignment(.center)
                    .padding(.top, 18)
                    .padding(.horizontal, 44)
                
                HStack {
                    Text(SuperHeroNameLocalized.textFieldPlaceholder)
                        .font(.system(size: 14))
                        .fontWeight(.light)
                    
                    Spacer()
                }
                .padding(.top, 45)
                
                RoundedRectangle(cornerRadius: 15)
                    .foregroundColor(HoHColors.textfieldForeground)
                    .frame(height: 40)
                    .overlay(TextField(SuperHeroNameLocalized.textFieldPlaceholder, text: $shnVM.superheroName)
                                .disableAutocorrection(true)
                                .padding(.horizontal, 14))
                
                Spacer()
                
                Button {
                    
                } label: {
                    
                    Text(CommonLocalized.nextCTA)
                        .fontWeight(.medium)
                        .font(.system(size: 16))
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 35)
                        .background( isActive ? LinearGradient(colors: [HoHColors.leftColor, HoHColors.rightColor], startPoint: .leading, endPoint: .trailing) : LinearGradient(colors: [HoHColors.disableButtonColor], startPoint: .leading, endPoint: .trailing))
                        .opacity(isActive ? 1.0 : 0.5)
                        .foregroundColor(.white)
                        .cornerRadius(100)
                    
                }
            }
            .padding(.horizontal, 16)
            .padding(.bottom, 45)
            .navigationBarHidden(true)
            .overlay(HStack{
                VStack {
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Image("Icon_LeftChevron")
                            .frame(width: 29, height: 29)
                            .padding(.leading, 16)
                            .foregroundColor(.black)
                            .padding(.leading, 10)
                            .padding(.top, 69)
                    }
                    Spacer()
                }
                Spacer()
            })
        }
    }
}

struct SuperHeroName_Previews: PreviewProvider {
    static var previews: some View {
        SuperHeroName()
    }
}

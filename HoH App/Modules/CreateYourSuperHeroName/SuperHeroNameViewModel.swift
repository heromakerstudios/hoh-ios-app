//
//  SuperHeroNameViewModel.swift
//  HoH App
//
//  Created by Omar Alejandro Regalado Mendoza on 31/01/22.
//

import Foundation
import Combine

class SuperHeroNameViewModel: ObservableObject {
    
    @Published var superheroName = ""
    @Published var isButtonActive = false
    
    init() {
        
    }
}

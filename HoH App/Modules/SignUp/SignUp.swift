//
//  SignUp.swift
//  HoH App
//
//  Created by Omar Alejandro Regalado Mendoza on 24/01/22.
//

import SwiftUI

struct SignUp: View {
    @State private var isAnimationComplete = false
    
    var body: some View {
        NavigationView {
            VStack {
                
                Image("Logo_HoH")
                    .onAppear {
                        self.isAnimationComplete = true
                    }
                    .offset(x: 0, y: self.isAnimationComplete ? 0 : 200)
                    .animation(Animation.easeIn.delay(0.8), value: self.isAnimationComplete)
                    
                Spacer()
                
                NavigationLink(destination: SuperHeroName().navigationBarHidden(true)) {
                    Text(SignUpLocalized.createYourHeroesCTA)
                        .fontWeight(.medium)
                        .font(.system(size: 16))
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: 35)
                        .background(LinearGradient(colors: [HoHColors.leftColor, HoHColors.rightColor], startPoint: .leading, endPoint: .trailing))
                        .foregroundColor(.white)
                        .cornerRadius(100)
                        .opacity(self.isAnimationComplete ? 1.0 : 0.0)
                        .animation(Animation.easeIn.delay(1.0), value: self.isAnimationComplete)
                }
                
                HStack {
                    
                    Text(SignUpLocalized.alreadyHaveAccount)
                        .font(.system(size: 14))
                    
                    Button {
                        
                    } label: {
                        Text(SignUpLocalized.sighInCTA)
                            .font(.system(size: 14))
                            .underline()
                    }
                }
                .padding(.top, 10)
                .opacity(self.isAnimationComplete ? 1.0 : 0.0)
                .animation(Animation.easeIn.delay(1.0), value: self.isAnimationComplete)
            }
            .navigationBarHidden(true)
            .padding(.vertical, 75)
            .padding(.horizontal, 16)
            .preferredColorScheme(.light)
        }
    }
}

struct SignUp_Previews: PreviewProvider {
    static var previews: some View {
        SignUp()
    }
}

//
//  SignUpLocalizedStrings.swift
//  HoH App
//
//  Created by Omar Alejandro Regalado Mendoza on 31/01/22.
//

import Foundation
import UIKit


struct SignUpLocalized {
    static let createYourHeroesCTA  = "create_your_hero_CTA".transferLocalizedString()
    static let alreadyHaveAccount = "already_have_account".transferLocalizedString()
    static let sighInCTA = "signin_CTA".transferLocalizedString()
}

struct SuperHeroNameLocalized {
    static let screenTitle = "createHero_Title".transferLocalizedString()
    static let screenSubTitle = "superheroName_Title".transferLocalizedString()
    static let screenBody = "superheroname_Body".transferLocalizedString()
    static let textFieldPlaceholder = "superHeroName_TextField_Placeholder".transferLocalizedString()
}

struct CommonLocalized {
    static let nextCTA = "next_CTA".transferLocalizedString()
}

extension String {
    
    public func transferLocalizedString() -> String {
        return NSLocalizedString(self, tableName: "HoH_Strings", bundle: Bundle.main, value: "", comment: "")
    }
}

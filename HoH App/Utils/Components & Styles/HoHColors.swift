//
//  HoHColors.swift
//  HoH App
//
//  Created by Omar Alejandro Regalado Mendoza on 25/01/22.
//

import Foundation
import SwiftUI

struct HoHColors {
    static let leftColor = Color(red: 209 / 255 , green: 75 / 255, blue: 75 / 255)
    static let rightColor = Color(red: 74 / 255, green: 12 / 255, blue: 134 / 255)
    static let textfieldForeground = Color(red: 232 / 255, green: 232 / 255, blue: 232 / 255)
    static let disableButtonColor = Color(red: 108 / 255, green: 28 / 255, blue: 120 / 255)
}

//
//  HoH_AppApp.swift
//  HoH App
//
//  Created by Omar Alejandro Regalado Mendoza on 24/01/22.
//

import SwiftUI

@main
struct HoH_AppApp: App {

    var body: some Scene {
        WindowGroup {
            SignUp()
        }
    }
}
